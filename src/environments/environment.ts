// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDhfD6ePcfNpDrwp9T94cnr4MenwwtDOeY",
    authDomain: "photostreamstorage.firebaseapp.com",
    databaseURL: "https://photostreamstorage.firebaseio.com",
    projectId: "photostreamstorage",
    storageBucket: "photostreamstorage.appspot.com",
    messagingSenderId: "1027428999158"
    }

};

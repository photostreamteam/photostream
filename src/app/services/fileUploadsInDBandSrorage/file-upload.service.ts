import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { count } from 'rxjs/operators/count';

import {UserServiceService} from '../user-service/user-service.service';
import {AuthService} from '../oAuth/auth-service.service';

import {Post} from '../../objects/Post';

@Injectable()
export class FileUploadService {

  private userDBkeyForImageUploads;// *** IMP

  private uid;
  public listOfUploads:[{"contentType":string,"uploadId":string,"parentPath":any[],"fullPath":string,"fileUrl":string,"filePathInStorage":string}]; // it will be cleared when all files uploaded.  
  private post; // it will be cleared when post is uploaded.

  private myObject;
  constructor(private http:HttpClient,private db:AngularFireDatabase,private userService:UserServiceService,
              private afStorage: AngularFireStorage) {
   }

// It checks the user object in uploads and contentType(images/videos) folder, creates it if not exist. 
verifyAllAndGetUserUploadsKeyInDb(path,url,contentType,numberOfUploads){

  this.uid=this.userService.userInfo.uid;
  
  let actualThis=this;

  actualThis.db.database.ref('/allUploads').orderByChild('uid').equalTo(actualThis.uid).once('value',snap=>{
    let allUploadsArrayByUser=actualThis.objectToArray(snap.val());
    
    if(allUploadsArrayByUser==undefined || allUploadsArrayByUser==null || allUploadsArrayByUser.length<=0){ //if User uploads unavailable, creates new one.


      actualThis.db.list('/allUploads').push({
        "uid":actualThis.uid,
        "images":{"count":0},
        "videos":{"count":0}
      }).then(value=>{

        allUploadsArrayByUser.push({dbId:value.key});

        // all Done.. proceed to add file details.
        this.addFileDetailsInDB(path,url,contentType,value.key,numberOfUploads);

        this.userDBkeyForImageUploads=value.key;//** IMP

      });
      
    }else{ //if upload Object for uid is already Exists, it just adds files to the content

      this.userDBkeyForImageUploads=allUploadsArrayByUser[0].dbId;//** IMP

      actualThis.db.database.ref('/allUploads/'+allUploadsArrayByUser[0].dbId+'/'+contentType).once('value',snap=>{

        let contentObj=snap.val();

        if(contentObj==null || contentObj==undefined){
          // ("user is available bt no content");
          
          actualThis.db.object('/allUploads/'+allUploadsArrayByUser[0].dbId+'/'+contentType).set({'count':0}).then(value=>{
            // ("created Content for "+contentType );

       // all Done.. proceed to add file details.
        this.addFileDetailsInDB(path,url,contentType,allUploadsArrayByUser[0].dbId,numberOfUploads);

          }) 
        }
      else{        
        // all Done.. proceed to add file details.
        this.addFileDetailsInDB(path,url,contentType,allUploadsArrayByUser[0].dbId,numberOfUploads);
      }

      });
    }
  }).catch(err=> console.log(err))
}

// It will be called by other component pages.
saveFileWithPathAndUrlAndContent(path,url,contentType,numberOfUploads){
  this.verifyAllAndGetUserUploadsKeyInDb(path,url,contentType,numberOfUploads);
}

// To save uploaded files details in DB for each user.
addFileDetailsInDB(filePath,fileUrl,contentType,userDBkey,numberOfUploads){
  this.uid=this.userService.userInfo.uid;
  
 let date=new Date();
  let data={
      "path":filePath,
      "url":fileUrl,
      "timeStamp":date.toString(),
      "active":true
  }

  this.db.database.ref('/allUploads/'+userDBkey+'/'+contentType+'/all').push(data).then(value =>{
  let count=0;

    let uploadId=value.key;
    let path=value.path["pieces_"];
    let fullPath="";
    
    for(let i=0;i<path.length;i++){
    fullPath=fullPath+"/"+path[i];
    }
    let oneAttachment={"contentType":contentType,"uploadId":uploadId,"parentPath":path,"fullPath":fullPath,"fileUrl":fileUrl,"filePathInStorage":filePath};
   // adding uploaded file dbId and full Path  details to listOdUploads, to use for creating POST, this is same Prototype of 
  if(this.listOfUploads==null || this.listOfUploads==undefined || this.listOfUploads.length<=0)
     this.listOfUploads=[oneAttachment]; 
  else
    this.listOfUploads.push(oneAttachment);

  //TODO
  //Getting current count, adding 1 and uploading again... 
  //But Need to update directly by adding 1 to the existing length of user.photo/user.video array.
  //Not sure when uploading image before user.photo/user.video arrays get initialized.
  this.db.database.ref('/allUploads/'+userDBkey+'/'+contentType+'/count').once('value',snap=>{
        count=snap.val();
        count++;

        this.db.database.ref('/allUploads/'+userDBkey+'/'+contentType).update({"count":count});    

        if(this.listOfUploads.length==numberOfUploads){
            this.preparePost(null,"afterAllUploads");          
        }
          
      }).catch(err=>{
        console.log(err)
      })
  })
  
}

// Converts Objcet to Arrays. and will add keys of the Object to the the array elements as bdId.
objectToArray(obj){
  if(obj==undefined || obj==null){
    return [];
  }
  let array=Object.keys(obj).map(function(e){
    obj[e].dbId=e;
    return obj[e];
  })
return array;
}

doSomeIntialDataSetupForAllNewUsers(){
  // add some initial setup for data creation,for all new Users 
}

//To get all uploads by loggedIn user's uid
getAllUploadsOfUser(){

  this.uid=this.userService.userInfo.uid;
  

    if(this.userService.userInfo.uid==null || this.userService.userInfo.uid==undefined){
      return;
    }

    this.db.database.ref('/allUploads').orderByChild('uid').equalTo(this.userService.userInfo.uid).on('value',snap=>{

    if(snap.val()==null || snap.val()==undefined){
      return;
    }

    this.userDBkeyForImageUploads=Object.keys(snap.val())[0]; //** IMP
    
    let value=Object.keys(snap.val()).map(e=>{ return snap.val()[e]})

    let listOfImages=this.objectToArray((value[0].images!=undefined && value[0].images.all != undefined) ? value[0].images.all :[]);
      this.userService.user.images=listOfImages;

    let listOfVideos=this.objectToArray((value[0].videos!=undefined && value[0].videos.all  != undefined) ? value[0].videos.all :[]) ;
      this.userService.user.videos=listOfVideos;
    })


}

deleteFileDetailsInDBAndStorage(fileStoragePath,uploadId,contentType){

  this.uid=this.userService.userInfo.uid;  

    this.afStorage.ref(fileStoragePath).delete().toPromise().then(_=>{
      //deleted In Storage success.
      // delete in DB now...
      
      if(this.userDBkeyForImageUploads==undefined || this.userDBkeyForImageUploads==null || this.userDBkeyForImageUploads.trim()==""){
        // TODO
        // Find the upload obj for User then delete file..
        // this.db.database.ref('/allUploads/').orderByChild('uid').equalTo(this.uid).

      }
      else{
        // TODO
        //1. delete this uploadDetails in attachments List of related postId.
        //2. Remove uploadId  

        this.db.object('/allUploads/'+this.userDBkeyForImageUploads+"/"+contentType+"/all/"+uploadId).query.once('value',(snap)=>{
          let oneUploadObj=snap.val();

          let postId=oneUploadObj.postId;


          this.db.object('/allPosts/'+postId+"/attachments").query.once('value',snap1=>{
            
            let attachmentsList=snap1.val();
            for(var j=0;j<attachmentsList.length;j++){
                if(attachmentsList[j].uploadId==uploadId){
                  attachmentsList.splice(j,1);
                  break;
                }
            }
            // removed related uploadIn post's attachmentsList and Updating the object in DB.
             this.db.object('/allPosts/'+postId+"/attachments").set(attachmentsList).then(_=>{
                //updated attachments obj... 

                //removing the actual upload in allUploads.
                this.db.object('/allUploads/'+this.userDBkeyForImageUploads+"/"+contentType+"/all/"+uploadId).remove().then(_=>{
                let count=this.userService.user[contentType].length;
                  this.db.object('/allUploads/'+this.userDBkeyForImageUploads+"/"+contentType+"/count").set(count);    
              })

             });

          })

        })
        
          
      }

    }).catch(err=> console.log(err));

}


preparePost(post:Post,from:string){

  if(from=="uploadPage"){
      this.listOfUploads=null;
      this.post=post;
      // If no attachments, create post directly..
      if(post.attachments==undefined || post.attachments.length<=0)
        {
          this.createPost(post);
        }
        else if(post.attachments.length>0){
            //Upload all Files, then create a post.

              for(let i=0;i<post.attachments.length;i++){
                this.saveFileWithPathAndUrlAndContent(post.attachments[i].filePathInStorage,post.attachments[i].fileUrl,post.attachments[i].contentType,post.attachments.length);
              }
        }

    }
    else if(from=="afterAllUploads"){
      post=this.post;      

        post.attachments=this.listOfUploads;
          this.createPost(post);
    }
}

createPost(post:Post){

  post.postedOn=(new Date()).toString();
  post.lastUpdateStamp=(new Date()).toString();

  this.db.list("/allPosts").push(post).then((data)=>{
    let postId=data.key;
    if(post.attachments!=undefined && post.attachments.length>0 ){

        for(let i=0;i<post.attachments.length;i++){

          this.db.object(post.attachments[i].fullPath).update({"postId":postId}).then((value)=>{
            //updated postId in uploads 
          }).catch(err=> {console.log("unable to update postId in each Upload")});

        }


    }
  }
);

}

getAllNewsFeed(count){
  return this.db.list('/allPosts').query.limitToLast(count);  
}

getMyposts(count){
  this.uid=this.userService.userInfo.uid;
  return this.db.list('/allPosts').query.orderByChild("ownerId").equalTo(this.uid).limitToLast(count);
}


}

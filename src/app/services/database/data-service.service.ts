import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class DataServiceService {

  private myObject;
  constructor(private http:HttpClient,private db:AngularFireDatabase) { }

getUsers(){
      return this.db.database.ref("/users/").orderByChild('name');
}

addNewUser(newName){
      return this.db.list('users').push({"name":newName});
}

searchUserByName(name){
  if(name==undefined || name==null || name.trim()=="")
      return this.db.database.ref("/users/").orderByChild('name');
  else
      return this.db.database.ref("/users/").orderByChild('name').equalTo(name);  
      
}

getAsObject(){
  return this.db.database.ref("");
}





}

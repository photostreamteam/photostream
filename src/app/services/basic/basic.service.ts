import { Injectable } from '@angular/core';

@Injectable()
export class BasicService {

  constructor() { }

  getFormattedDate(date)
  {
    if(date!=undefined)
    {
      date=new Date(date);
      return date.getHours()+":"+date.getMinutes()+" on "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear();    
    }
  }
  
}

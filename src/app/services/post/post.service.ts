import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { promise } from 'selenium-webdriver';
import { Observable } from '@firebase/util/dist/esm/src/subscribe';

import { UserServiceService} from '../user-service/user-service.service';
import { FileUploadService} from '../fileUploadsInDBandSrorage/file-upload.service';

@Injectable()
export class PostService {

  private uid;
  constructor(private userService:UserServiceService,private db:AngularFireDatabase,private fileUploadService:FileUploadService) { 
  }

doLike(postId,post_ownerId){
    // TODO 
    // 1. Add Likes Objest to All Likes List of the related Post id in 
    // 2. Add one notification entry in post's owner's notificationList 
      // we can get likes list with post Id in allLikes/postId/likesList....
// 1.
      let date=(new Date()).toString();

  let likeObject={
      uid:this.userService.userInfo.uid,
      "timeStamp":date,
      "postId":postId
  }

  this.db.list('/allLikes/'+postId+'/likesList').push(likeObject).then(data=>{
      //likes List Created, then give Owner a notification..
      
      // 2.
              // No Notification if, user likes his own post..
      if(this.userService.userInfo.uid != post_ownerId )
  {
      let notificationObject={
        "postId":postId,
        "likedBy":this.userService.userInfo.uid,
        "type":"like",
        "timeStamp":date,
        "unread":true,
      }

      this.db.list('/allNotifications/'+post_ownerId+'/list/').push(notificationObject).once('value',snap=>{
          //added to Notification List.
      }).catch(err=>{console.log("unable to add to notifications list")})
  }

  })

}

doDisLike(postId){

  // TODO
  // 1. Remove current User ID from Liked List of a post
  // allLikes/{postId}/likesList/[....]

  this.db.list('/allLikes/'+postId+'/likesList').query.orderByChild('uid').equalTo(this.userService.userInfo.uid).once('value',snap=>{

    let dbId=Object.keys(snap.val())[0];

    this.db.list('/allLikes/'+postId+'/likesList/'+dbId).remove().then(data=>{
      // updated likes List by removing uid form List
    })

  })


}


isPropertyValueExistsInArrayOfObjects(property,value,array){

  let result=false;

  for(let i=0;i<array.length;i++){
      let oneObj=array[i];  
    if(oneObj[property]==value)
    {
     result=true;
     break;  
    }  
  }
  return result;
}

getPostsUnreadNotifications(uid){
    return this.db.list('/allNotifications/'+uid+"/list").query.orderByChild("unread").equalTo(true);
   }


getLikesList(postId){
  return this.db.list('/allLikes/'+postId+'/likesList').query;
}


addComment(commentData){
    // TODO 
    // 1. Add Comments Objest to All Comments List of the related Post id 
    // 2. Add one notification entry in post's owner's notificationList 
      // we can get comments list with post Id in allComments/postId/commentsList....

    //  1.
    let date=(new Date()).toString();
    
      let commentObject={
          uid:this.userService.userInfo.uid,
          "timeStamp":date,
          "postId":commentData.postId,
          "comment":commentData.comment
      }
    
      this.db.list('/allComments/'+commentData.postId+'/commentsList').push(commentObject).then(data=>{
        //likes List Created, then give Owner a notification..
        
        // 2.
                // No Notification if, user comment on his own post..
        if(this.userService.userInfo.uid != commentData.post_ownerId )
    {
        let notificationObject={
          "postId":commentData.postId,
          "by":this.userService.userInfo.uid,
          "type":"comment",
          "timeStamp":date,
          "unread":true,
        }
  
        this.db.list('/allNotifications/'+commentData.post_ownerId+'/list/').push(notificationObject).once('value',snap=>{
            //added to Notification List.
        }).catch(err=>{console.log("unable to add to notifications list")})
    }
  
    })  
}

editComment(){
//TODO 
// 1.Edit a comment which is created by same user
}

deleteComment(){
// TODO
// 1.Delete a comment which is created by same user
}

getCommentsList(postId){
  return this.db.list("/allComments/"+postId+"/commentsList").query;
}


}
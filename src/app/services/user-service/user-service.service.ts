import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../OAuth/auth-service.service';

import {HttpClient} from '@angular/common/http';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFireDatabase } from 'angularfire2/database';
import { map } from 'rxjs/operators/map';
import { finalize } from 'rxjs/operators';
declare var $:any;

@Injectable()
export class UserServiceService {
private photos:[{id:"",url:""}];
public user={images:[],videos:[],connections:[{}],posts:[{}],own:{cover:"assets/library/(1).jpg"}};

public isLogin:boolean;
public userDetails:firebase.User=null;

public userInfo;
private allUsers;

  constructor(private http:HttpClient,private db:AngularFireDatabase,private afStorage: AngularFireStorage) {
    // this.user.photos=[ {id:1,url:"assets/library/(1).jpg"},
    //                    {id:2,url:"assets/library/(2).jpg"},
    //                    {id:3,url:"assets/library/(3).jpg"},
    //                    {id:4,url:"assets/library/(4).jpg"},
    //                    {id:5,url:"assets/library/(5).jpg"},
    //                    {id:6,url:"assets/library/(6).jpg"},
    //                    {id:7,url:"assets/library/(7).jpg"},
    //                    {id:8,url:"assets/library/(8).jpg"},
    //                    {id:9,url:"assets/library/(9).jpg"},
    //                   {id:10,url:"assets/library/(10).jpg"},
    //                   {id:11,url:"assets/library/(11).jpg"},
    //                   {id:12,url:"assets/library/(12).jpg"},
    //                   {id:13,url:"assets/library/(13).jpg"},
    //                   {id:14,url:"assets/library/(14).jpg"},
    //                   {id:15,url:"assets/library/(15).jpg"},
    //                   {id:16,url:"assets/library/(16).jpg"},
    //                   {id:17,url:"assets/library/(17).jpg"},
    //                   {id:18,url:"assets/library/(18).jpg"},
    //                   {id:19,url:"assets/library/(19).jpg"},
    //                   {id:20,url:"assets/library/(20).jpg"},
    //                   {id:21,url:"assets/library/(21).jpg"},
    //                   {id:22,url:"assets/library/(22).jpg"},
    //                   {id:23,url:"assets/library/(23).jpg"}];
                     
              this.user.connections=[{connectionId:'0001',userId:'102',devices:["Canon 80D"],userName:"abhilash",displayName:"Abhilash Varma",pic:"assets/profiles/abhi.jpg"},
                                     {connectionId:'0002',userId:'103',devices:["Canon 80D"],userName:"chaithanya",displayName:"Chaithanya",pic:"assets/profiles/chaithu.jpg"},
                                     {connectionId:'0003',userId:'104',devices:["Canon 80D"],userName:"adhi",displayName:"Adhi sena",pic:"assets/profiles/adhi.jpg"},
                                     {connectionId:'0004',userId:'105',devices:["Canon 80D"],userName:"kiran",displayName:"Kiran kumar",pic:"assets/profiles/kiran.jpg"},
                                     {connectionId:'0005',userId:'106',devices:["Canon 80D"],userName:"santhosh",displayName:"Santhosh kumar",pic:"assets/profiles/santhu.jpg"},
                                     {connectionId:'0006',userId:'107',devices:["Canon 80D"],userName:"karthik",displayName:"Karthik kumar",pic:""},
                                     {connectionId:'0007',userId:'108',devices:["Canon 80D"],userName:"nikhil",displayName:"Nikhil",pic:"assets/profiles/nikhil.jpg"},
                                     {connectionId:'0008',userId:'109',devices:["Canon 80D"],userName:"eshwar",displayName:"Eshwar Lal",pic:""},
                                     {connectionId:'0009',userId:'110',devices:["Canon 80D"],userName:"raju",displayName:"Rajesh Raju ",pic:""},
                                     {connectionId:'0010',userId:'111',devices:["Canon 80D"],userName:"prashanth",displayName:"Prashanth",pic:""},
                                     {connectionId:'0011',userId:'112',devices:["Canon 80D"],userName:"sathish",displayName:"Sathish",pic:"assets/profiles/sathish.jpg"}, 
                                    ];
   }

getUser(){
  return this.user;
}

isUserLogin()
{
 return this.isLogin;
}

getDefaultValues(value){
  if(value==null || value==undefined)
    return "";
  else
    return value;
}

isUserNew(){

  let uid=this.userDetails.uid;
  this.db.object('/users/'+uid).query.once('value',snap=>{
          let result =snap.val();
          if(result ==null || result==undefined){
      
              this.userInfo={
                  "uid":this.userDetails.uid,
                  "email":this.userDetails.email,
                  "displayName":this.userDetails.displayName,
                  "phone":this.userDetails.phoneNumber,
                  "photoUrl":this.userDetails.photoURL,
                }

                this.db.object('/users/'+uid).update(this.userInfo).then((data)=>{
                  }).catch(err=> console.log(err))
                  this.loadAllIntialData();
                }
                else{
                  //  console.log("user Exist")
                  this.userInfo=snap.val();
                  this.loadAllIntialData();
                } 

  })  

}

getUserDetailsById(id){
  //getting user details when requested...
  return this.db.object('/users/'+id).query;
}

getAllUserDetails(){
  //loading all Users is risky...
  // this.db.list("/users").query.on('value',snap=> this.allUsers=snap.val());
}

loadAllIntialData(){
  //all notifications, allmsgs count ..etc  
}
  

}

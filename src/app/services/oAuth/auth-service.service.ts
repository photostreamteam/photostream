import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

declare var $: any;

import {UserServiceService } from '../user-service/user-service.service'

@Injectable()
export class AuthService {
  private user: Observable<firebase.User>;
  // public userDetails: firebase.User = null;
constructor(private firebaseAuth: AngularFireAuth, private router: Router,private userService:UserServiceService) { 
      this.user = firebaseAuth.authState;
this.user.subscribe(
        (user) => {
          if (user) {
            this.userService.userDetails = user;     
            this.userService.isLogin=true;    
            this.userService.isUserNew();
            this.userService.getAllUserDetails();
           }
          else {
            this.userService.userDetails = null;
            this.userService.isLogin=false;
          }
        }
      );
  }

signInWithGoogle() {

  let authPop=this.firebaseAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    ).then((res) => this.router.navigate(['/updates']));
    return authPop;

  }
isLoggedIn() {
  if (this.userService.userDetails == null ) {
      return false;
    } else {
      return true;
    }

  }
logout() {
    this.firebaseAuth.auth.signOut().then(()=> {this.userService.userDetails=null;this.userService.isLogin=false;})
    .then(() => this.router.navigate(['/home'])).then(()=>{console.log(this.userService.isUserLogin());  this.userService.user=null;
      this.reloadPage();
    } );
  }

  reloadPage(){
    window.location.reload()
  }  
}
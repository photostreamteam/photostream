import { Injectable } from '@angular/core';
declare var $: any;

@Injectable()
export class NotificationService {
showNotification:boolean=false;

  constructor() { }

  openNotification(){
    this.showNotification=true;
    let actualThis=this;
    setTimeout(function(){
      actualThis.showNotification=false;
  },15000)
}

fillCoverPic(pic){
  if(pic!=undefined && pic !=null)
  {
      $("#bio").css("background-image","url('"+pic+"')");
      $("#sideNavBarCover").css("background-image","url('"+pic+"')");

      localStorage.coverPic=pic;
  }
}


}

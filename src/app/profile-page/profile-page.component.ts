import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { map } from 'rxjs/operators/map';

import { Observable } from 'rxjs/Observable';
import { DataSnapshot } from '@firebase/database-types';
import { forEach } from '@firebase/util/dist/esm/src/obj';

import { DataServiceService} from '../services/database/data-service.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { NotificationService } from '../services/notification/notification.service';
import { FileUploadService} from '../services/fileUploadsInDBandSrorage/file-upload.service';
import { UserServiceService} from '../services/user-service/user-service.service';
import {BasicService } from '../services/basic/basic.service';

import { User } from '../objects/User';
import { Post} from '../objects/Post';

declare var $:any;

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  users;

  newName:string="";

  imageUrl:Observable<string>;
  imageTempUrl=[];

  getUrlFromPath:Observable<string>;
  imageRef:string;

  searchName:string;

  private postCount=10;
  private lastTimeLoadedPostsCount=0;
  public isLoadingFeed=false;

  public allPosts:Post[]=[];
  
  constructor(private afStorage: AngularFireStorage,private dataService:DataServiceService,
              private fdb:AngularFireDatabase,public userService:UserServiceService,private notificationService:NotificationService,
            private db:AngularFireDatabase,private fileUploadService:FileUploadService,private basicService:BasicService) { 

              }

  ngOnInit(){   
    this.dataService.getUsers().on('value',snap=>{
      this.users=this.objectToArray(snap.val()); 
      this.notificationService.openNotification();

      this.fetchNewsFeed();
    });

    let actualThis=this;
    
          $(document).off('scroll'); //removing all other pages onScroll event in this page
    
          $(document).on('scroll',function(){
            let fullHeight=$(document).height();
            let windowHeight=window.innerHeight;
    
            let minHeightToReload=fullHeight-windowHeight-300;
            let scrollHeight=window.pageYOffset;
    
            if(actualThis.isLoadingFeed==false && minHeightToReload>500 && scrollHeight>minHeightToReload){
              actualThis.fetchNewsFeed();
            }
            
        })  
  }

searchUsers(){
  this.dataService.searchUserByName(this.searchName).on('value',snap=>{
    this.users=this.objectToArray(snap.val()); 
  });

}

  saveData(){
    let obj={"name":this.newName};
    this.dataService.addNewUser(this.newName).then(function(){
    })
  }

  objectToArray(obj){
    if(obj==undefined || obj==null){
      return [];
    }
    let array=Object.keys(obj).map(function(e){
      obj[e].dbId=e;
      return obj[e];
    })
  return array;
  }

fetchNewsFeed(){
  this.isLoadingFeed=true;
  this.fileUploadService.getAllNewsFeed(this.postCount).on('value',snap=>{

    let fullData=this.fileUploadService.objectToArray(snap.val()).reverse();// Trying to show the last post at the top...
    let tempPost=[];

        if(this.allPosts.length < fullData.length){
          tempPost=fullData.slice(this.allPosts.length); //when scrooling..
        }else if(this.allPosts.length == fullData.length)  {

            // if length is equal, there are 2 chance
            // 1. reached last post.
            // 2. New post created and then new list of posts came with same length.
        // 1.
            let length=this.allPosts.length;
            if((length>0) && (this.allPosts[0].dbId == fullData[0].dbId)){
              this.isLoadingFeed=false;      
                return;
            }

            // remaining is 2.

          tempPost=fullData.slice(0);  //when adding new post, it will load same number of posts, but with updated posts.
          this.allPosts=[];
        }

    if(this.lastTimeLoadedPostsCount==fullData.length)
    {
      this.isLoadingFeed=false;      
      // return;
    }
    else{
      this.lastTimeLoadedPostsCount=tempPost.length;
      this.postCount+=10;  
    }

    tempPost.forEach((post)=>{
        this.userService.getUserDetailsById(post.ownerId).once('value',snap1=>{
          post.ownerDetails=snap1.val();
        })

        post.postedOn=this.basicService.getFormattedDate(post.postedOn);
        // post.attachments=this.fileUploadService.objectToArray(post.attachments);
      })
      this.allPosts=this.allPosts.concat(tempPost);
  
      this.isLoadingFeed=false;      
  })
}

}
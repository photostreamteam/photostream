import { Component, OnInit } from '@angular/core';
declare var $: any;

import{UserServiceService} from "../../services/user-service/user-service.service";
import{NotificationService} from '../../services/notification/notification.service';
import{FileUploadService} from '../..//services/fileUploadsInDBandSrorage/file-upload.service';
import{BasicService} from '../../services/basic/basic.service';

import {Post} from '../../objects/Post';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

public user;
public openedImageUrl;
public cover;

public imageRef;

public allMyPosts:Post[]=[];

private postCount=10;
private lastTimeLoadedPostsCount=0;
public isLoadingFeed=false;
  
  constructor(public userService:UserServiceService,public notificationService:NotificationService, public fileUploadService:FileUploadService,
  private basicService:BasicService) {
    this.user=userService.getUser();
  }

  ngOnInit() {    
    this.imageRef=this.userService.userDetails.email;

             this.cover=localStorage.coverPic;             
             this.notificationService.fillCoverPic(this.cover);
             this.fileUploadService.getAllUploadsOfUser();  //this will assign photos to user property indirectly.                         

            this.fetchMyOwnPosts();    
            
            $(document).off('scroll'); //removing all other pages onScroll event in this page
            
            let actualThis=this;

            $(document).on('scroll',function(){
                let fullHeight=$(document).height();
                let windowHeight=window.innerHeight;

                let minHeightToReload=fullHeight-windowHeight-300;
                let scrollHeight=window.pageYOffset;

                if(actualThis.isLoadingFeed==false && minHeightToReload>500 && scrollHeight>minHeightToReload){
                  actualThis.fetchMyOwnPosts();
                }

            })
  }

getProfilePicUrl(url){
  
if(url && url.length>0)
  return url;
else
  return "assets/user.png";
}

callMe(url){
  this.openedImageUrl=url;
}

setCover(url){
  this.cover=url;
  this.notificationService.fillCoverPic(this.cover);
}

toggleExtraOptionsMenu(index,contentType){
  $(".allMenuDivs").css("display","none");  
  // $("#extraMenu_"+index).fadeToggle(50); 

  if($("#extraMenu_"+contentType+"_"+index).hasClass('extraMenuActive'))
  { 
    $("#extraMenu_"+contentType+"_"+index).fadeOut(50); 
    $("#extraMenu_"+contentType+"_"+index).removeClass('extraMenuActive')
  }else{

    $(".extraMenuActive").removeClass("extraMenuActive");    

    $("#extraMenu_"+contentType+"_"+index).fadeIn(50);
    $("#extraMenu_"+contentType+"_"+index).addClass('extraMenuActive')     
  }


  }

edit(dbId,contentType){
  // console.log(dbId,contentType);
}

deleteFile(path,dbId,contentType )
{
this.fileUploadService.deleteFileDetailsInDBAndStorage(path,dbId,contentType);
}

fetchMyOwnPosts(){
  this.isLoadingFeed=true;
  
  this.fileUploadService.getMyposts(this.postCount).on('value',snap=>{

    let fullData=this.fileUploadService.objectToArray(snap.val()).reverse();// Trying to show the last post at the top...
    let tempPost=[];


        if(this.allMyPosts.length < fullData.length){
          tempPost=fullData.slice(this.allMyPosts.length); //when scrooling..
        }else if(this.allMyPosts.length == fullData.length)  {

          
            // if length is equal, there are 2 chance
            // 1. reached last post.
            // 2. New post created and then new list of posts came with same length.
        // 1.
        let length=this.allMyPosts.length;
        if((length>0) && (this.allMyPosts[0].dbId == fullData[0].dbId)){
          this.isLoadingFeed=false;      
            return;
        }

        // remaining is 2.

          tempPost=fullData.slice(0);  //when adding new post, it will load same number of posts, but with updated posts.
          this.allMyPosts=[];
        }


        if(this.lastTimeLoadedPostsCount==fullData.length)
        {
          this.isLoadingFeed=false;      
          // return;
        }
        else{
          this.lastTimeLoadedPostsCount=fullData.length;
          this.postCount+=10;
        }
        
        tempPost.forEach((post)=>{
            post.ownerDetails=this.userService.userInfo;
            post.postedOn=this.basicService.getFormattedDate(post.postedOn);
            // post.attachments=this.fileUploadService.objectToArray(post.attachments);
        })

        this.allMyPosts=this.allMyPosts.concat(tempPost);            
        this.isLoadingFeed=false;        
  })
}

}

import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { map } from 'rxjs/operators/map';
import { AngularFireDatabase } from 'angularfire2/database';
import { finalize } from 'rxjs/operators';
declare var $:any;

import{DataServiceService} from '../services/database/data-service.service';
import{FileUploadService} from '../services/fileUploadsInDBandSrorage/file-upload.service';
import{UserServiceService} from '../services/user-service/user-service.service';

import{Post} from '../objects/Post';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  selectedImages=[];
  reference: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress={};
  


  //to show the Image Url after uploading for only one file at a time
 @Input() downloadURL=[];
 @Output() downloadURLChange=new EventEmitter();

 //to show the selected Image Url before uploading for Multiple selection of image
 @Input() localURL=[];
 @Output() localURLChange=new EventEmitter();
  state;

 @Input() imagePath:string;
 @Output() imagePathChange=new EventEmitter<string>();

  canUpload:boolean=false;
  public submitted=false;
  public invalidUploads=false;

  public fileMaxSizeForImages=2097152; //2MB in bytes
  public fileMaxSizeForVidoes=10485760; //10MB in bytes
  public uploadsLimit=2;
  public validationMsg="Only "+this.uploadsLimit+" files. Image size 2MB, video size 10MB.";
  public canShowValidation=false;

  public imageType=['image/jpeg','image/png'];
  public videoType=['video/mp4'];

  public post:Post=new Post();
  public privacyTypeList:[string]=["All","Only to","Except","Friends","Only me"];
  


  constructor(private afStorage: AngularFireStorage,private dataService:DataServiceService,
    private fdb:AngularFireDatabase,private fileUploadService:FileUploadService,private userService:UserServiceService) { }
  
    ngOnInit(){
      // this.post.attachments=[];
    }
    
takeOnlyLimitedFiles(files:[any]){
return files.splice(this.uploadsLimit);
}

    getFileDetails(event) {
      try{
      this.selectedImages=event.target.files;

      if(this.selectedImages.length>this.uploadsLimit)
      {
        this.canShowValidation=true;
        this.selectedImages=[];
      }


    for(let i=0;i<this.selectedImages.length;i++){ 
  
      this.uploadProgress[i]=0;
      let objectURL = window.URL.createObjectURL(this.selectedImages[i]); 

      this.canShowValidation=false;
      if(this.imageType.indexOf(this.selectedImages[i].type)>-1 && this.selectedImages[i].size>this.fileMaxSizeForImages){
        this.selectedImages=[];
        this.canShowValidation=true;
        break;        
      }

      this.canShowValidation=false;
      if(this.videoType.indexOf(this.selectedImages[i].type)>-1 && this.selectedImages[i].size>this.fileMaxSizeForVidoes){
        this.selectedImages=[];
        this.canShowValidation=true;        
        break;                
      }

      this.localURL.push(objectURL);
    }

      this.localURLChange.emit(this.localURL); // to send the Data to the parent Component
  // this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
    }catch(e){
        console.log(e);
      }
    }
  
  uploadFile(index){

    if(this.selectedImages==undefined || this.selectedImages==null || this.selectedImages.length==0){
      this.postTheData();
      return;
    }

this.submitted=true;

  if(index==undefined || index==null)
    index=0;

  let image=this.selectedImages[index];


  this.uploadProgress[index]=1;
    
this.imagePath=this.userService.userInfo.uid;

    if(this.imagePath==undefined){
      return;
    }

    let id = Math.random().toString(36).substring(2);    
    this.imagePath=this.imagePath+"/"+id;

  // var currentRunningNumber=a;
    var uploadTask =this.afStorage.storage.ref(this.imagePath).put(image);
    
    let actualThis=this;

    let contentType=actualThis.checkContentType(actualThis.selectedImages[index].type);
    if(contentType=="invalid")
      return;

    uploadTask.on('state_changed', function(snapshot){
      
      actualThis.uploadProgress[index]=(uploadTask.snapshot.bytesTransferred/uploadTask.snapshot.totalBytes)*100;        
    }, function(error) {
      console.log("error");      
     }, function() {
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        if(downloadURL!=undefined)
        {
          actualThis.downloadURL=downloadURL;
          // actualThis.downloadURLChange.emit(downloadURL); // to send the Data to the parent Component
          // actualThis.imagePathChange.emit(actualThis.imagePath);     
          let localContentType=actualThis.checkContentType(actualThis.selectedImages[index].type);
          if(localContentType=="invalid")
            return;

            
            // actualThis.fileUploadService.saveFileWithPathAndUrlAndContent(actualThis.imagePath,downloadURL,localContentType);

            let fileDetials={
              contentType:localContentType,
              filePathInStorage:actualThis.imagePath,
              fileUrl:downloadURL,
              uploadId:"",
              parentPath:[],
              fullPath:""
            };
        
              if(actualThis.post.attachments==undefined)
                actualThis.post.attachments=[fileDetials];
              else
                actualThis.post.attachments.push(fileDetials);

            if(actualThis.selectedImages!=null && actualThis.selectedImages!=undefined &&  index<actualThis.selectedImages.length-1)
                actualThis.uploadFile(index+1);

            if(actualThis.post.attachments.length==actualThis.selectedImages.length)
            {
              actualThis.postTheData();
            }

        }

      });
    });


  }


    checkProgress(value){

      if(value != null && value != undefined && value>0)
      { 
      return true;
      }
      else
       return false;
    }

    openFileSelection(){
      this.submitted=false;
      this.selectedImages=[];
      this.localURL=[];
      $("#fileUploadInput").click();
    }

    checkContentType(type){

      if(type==null || type==undefined || type.trim().length==0)
      {
        this.invalidUploads=true;        
        return "invalid";
      }
      if(this.imageType.indexOf(type)>-1)
          return "images";
      else if(this.videoType.indexOf(type)>-1)
        return "videos";
      
      else{
        this.invalidUploads=true;        
        return "invalid";
      } 
    }

typing(event){

}

setPrivacyType(privacy){
  this.post.privacy.type=privacy;
}

postTheData(){
  this.post.ownerId=this.userService.userInfo.uid;
  this.fileUploadService.preparePost(this.post,"uploadPage");
  this.post=new Post();
}

}

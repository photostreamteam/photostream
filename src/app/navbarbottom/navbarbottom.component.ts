import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../services/user-service/user-service.service';
import {NotificationService} from '../services/notification/notification.service';
import {PostService} from '../services/post/post.service';
import {FileUploadService} from '../services/fileUploadsInDBandSrorage/file-upload.service';

import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-navbarbottom',
  templateUrl: './navbarbottom.component.html',
  styleUrls: ['./navbarbottom.component.css']
})
export class NavbarbottomComponent implements OnInit {

private userFullName:string;
private photoUrl:string;
private emptyPhotoUrl="";
public allNotifications=[];

  public userDetails: Observable<firebase.User> = null;
  constructor(public userService:UserServiceService,public notificationService:NotificationService,private postService:PostService,
  private fileUploadService:FileUploadService) {
  }
  

  ngOnInit() {
    this.notificationService.fillCoverPic(localStorage.coverPic);

    this.postService.getPostsUnreadNotifications(this.userService.userDetails.uid).on('value',snap=>{
      this.allNotifications=this.fileUploadService.objectToArray(snap.val());
    })
    
  }

changeActiveStatus(elem){

let id;
if(document.querySelector(".list-group-item")){
 id= document.querySelector(".list-group-item").getAttribute("id");
document.querySelector("#"+id).classList.remove("list-group-item");
document.querySelector("#"+id).classList.remove("active");
}

if(elem){
  document.getElementById(elem).classList.add("list-group-item");
  document.getElementById(elem).classList.add("active");  
}

}

callMe()
{
// empty method to open side nav bar on mobile devices
}

openSubPage(name){
  // redirect to sub Page based On name
}

}

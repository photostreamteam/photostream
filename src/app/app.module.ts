import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy,HashLocationStrategy} from '@angular/common';
import { RouterStateSnapshot} from '@angular/router';

import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { NavbarbottomComponent } from './navbarbottom/navbarbottom.component';
import { HomeComponent } from './home/home.component';
import { MyProfileComponent } from './profile-page/my-profile/my-profile.component';


// Services
import { UserServiceService} from './services/user-service/user-service.service';
import { AuthService} from './services/OAuth/auth-service.service';
import { AuthGuard} from './services/OAuth/auth-guard.service';
import { BasicNavBarComponent } from './basic-nav-bar/basic-nav-bar.component';
import { DataServiceService } from './services/database/data-service.service';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { SafeImagePipe } from './pipes/safe-image.pipe';
import { NotificationService} from './services/notification/notification.service';
import { FileUploadService } from './services/fileUploadsInDBandSrorage/file-upload.service';
import { PostComponent } from './post/post.component';
import { BasicService} from './services/basic/basic.service';
import { PostService} from './services/post/post.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProfilePageComponent,
    NavbarbottomComponent,
    HomeComponent,
    MyProfileComponent,
    BasicNavBarComponent,
    ImageUploadComponent,
    SafeImagePipe,
    PostComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,ReactiveFormsModule, 
    AppRoutingModule,AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,AngularFireStorageModule,HttpModule,HttpClientModule
  ],
  providers: [{provide: LocationStrategy, useClass:HashLocationStrategy},UserServiceService,AuthService,AuthGuard,DataServiceService,
            NotificationService,FileUploadService,BasicService,PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }

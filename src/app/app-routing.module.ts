import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {NavbarComponent} from './navbar/navbar.component';
import {ProfilePageComponent} from './profile-page/profile-page.component';
import {MyProfileComponent} from './profile-page/my-profile/my-profile.component';
import {HomeComponent} from './home/home.component'
import {AuthGuard} from './services/OAuth/auth-guard.service';

const routes: Routes = [
  
  
  {path:"",
  redirectTo:"/updates",
  pathMatch:"full"
  },
  {
    path:"navbar",
    component:NavbarComponent,
    // outlet: "navbar"
  },
  {
    path:"home",
    component:HomeComponent
  },
  {
    path:"updates",
    component:ProfilePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path:"myprofile",
    component:MyProfileComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {NgForm,NgModel,NgControl} from '@angular/forms';

import {AuthService} from '../services/OAuth/auth-service.service'
import { UserServiceService } from '../services/user-service/user-service.service';

declare let $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

userId:string="sudheer";
password:string;
isLogin:boolean;

  constructor(private authService:AuthService,public userService:UserServiceService) { }

  ngOnInit() {
    this.isLogin=this.userService.isUserLogin();
    $(document).off('scroll');
  }

submitForm(myForm:NgForm){
console.log(myForm.controls)
}

loginWithGoogle(){
this.authService.signInWithGoogle();
}

}

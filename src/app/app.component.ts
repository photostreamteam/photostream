import { Component } from '@angular/core';
import {UserServiceService} from './services/user-service/user-service.service';
import {NotificationService} from './services/notification/notification.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
public isLogin:boolean;
  constructor(public userService:UserServiceService,public notificationService:NotificationService) { }
  
    ngOnInit() {
this.isLogin=this.userService.isUserLogin();
}
  
openNotif(){
  this.notificationService.openNotification();   
}

closeNotif(){
  this.notificationService.showNotification=false;
}
}
import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../services/user-service/user-service.service';
import {AuthService } from '../services/OAuth/auth-service.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

public showMainNav:boolean;

  constructor(public userService:UserServiceService,public authService:AuthService ) {
  }

  ngOnInit() {
this.showMainNav=this.authService.isLoggedIn();

  }

logout(){
  this.authService.logout();
}

}

import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

import {UserServiceService} from '../services/user-service/user-service.service';
import {PostService} from '../services/post/post.service';
import {FileUploadService} from '../services/fileUploadsInDBandSrorage/file-upload.service';
import { BasicService } from '../services/basic/basic.service';

import {Post} from '../objects/Post';

declare var $:any;

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  public imageRef;

  @Input() public post:any;
  @Output() public postChange=new EventEmitter();

  public like:any;

  public newComment="";
  public canShowAllLikes=false;
  public canShowAllComments=false;
  
  constructor(private userService:UserServiceService,private postService:PostService,private fileUploadService:FileUploadService,
  private basicService:BasicService) { }

  ngOnInit() {
    let actualThis=this;
      this.postService.getLikesList(this.post.dbId).on('value',snap=>{
          this.post.likesList=this.fileUploadService.objectToArray(snap.val());

          this.post.likesList.forEach((like)=>{
            this.userService.getUserDetailsById(like.uid).once('value',snap1=>{
              like.ownerDetails=snap1.val();
            })
            like.timeStamp=this.basicService.getFormattedDate(like.timeStamp);    
        })
          actualThis.like = this.postService.isPropertyValueExistsInArrayOfObjects('uid',this.userService.userInfo.uid,this.post.likesList);
      });

  this.postService.getCommentsList(this.post.dbId).on('value',snap => {

    this.post.commentsList=this.fileUploadService.objectToArray(snap.val());
    
              this.post.commentsList.forEach((comment)=>{
                this.userService.getUserDetailsById(comment.uid).once('value',snap1=>{
                  comment.ownerDetails=snap1.val();
                })
                comment.timeStamp=this.basicService.getFormattedDate(comment.timeStamp);    
            })
  })

  }

  setHeightByRows(id,content){
    
      if(content==undefined || content==null || content.length<=0)
         return false;
     else{
        let tempArray=content.split("\n"); 
        let lettersCount=content.length;
        let rowsByLetters=1;
    
        if(lettersCount>50){
          let areaWidth=$("#postContent_"+id).width();
          let textFontSize=7; //width of each text approximately..
    
          rowsByLetters=Math.ceil(lettersCount/(Math.round(areaWidth/textFontSize)));
        }  
    
        let finalRows=(tempArray.length>rowsByLetters?tempArray.length:rowsByLetters)
    
        $("#postContent_"+id).prop("rows",finalRows+2);
        return true; 
     }
    }

  likeToggle(value){
    this.like=value;

    if(value==true){
      this.postService.doLike(this.post.dbId,this.post.ownerId);
    }else{
      this.postService.doDisLike(this.post.dbId);
    }

  }
  toggleShowAllLikes(){
    this.canShowAllLikes=!this.canShowAllLikes;
  }

toggleShowAllComments(){
  this.canShowAllComments=!this.canShowAllComments;  
}

  addComment(){
    var commentObj ={
      "comment":this.newComment,
      "postId":this.post.dbId,
      "post_ownerId":this.post.ownerId
    }

    if(this.newComment != undefined && this.newComment.trim().length>0){
      this.postService.addComment(commentObj);
      this.newComment="";
    }
  }


}

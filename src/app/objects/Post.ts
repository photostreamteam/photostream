export class Post{

content:string="";
ownerId:any="";
ownerDetails:any;
postedOn:string;
lastUpdateStamp:string;           
locations:string;
dbId:string;

tags:[any];
privacy:{"type":string,"listOfPeople":[string]}={type:"All",listOfPeople:null};

attachments:[{"contentType":string,"uploadId":string,"parentPath":any[],"fullPath":string,"fileUrl":string,"filePathInStorage":string}];
// attachments:any[]=[];

}
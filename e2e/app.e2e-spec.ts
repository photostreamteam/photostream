import { SocialNetworkPage } from './app.po';

describe('social-network App', () => {
  let page: SocialNetworkPage;

  beforeEach(() => {
    page = new SocialNetworkPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
